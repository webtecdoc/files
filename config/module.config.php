<?php
/**
 * Created by PhpStorm.
 * User: Alexey Orlov <aorlov@cpan.org>
 */

return array(
    'controllers'   => array(
        'invokables'    => array(
            'Files\Controller\File' => 'Files\Controller\FileController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'album' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/files[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Files\Controller\File',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'album' => __DIR__ . '/../view',
        ),
    ),
);